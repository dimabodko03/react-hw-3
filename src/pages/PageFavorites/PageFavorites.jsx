import PropTypes from "prop-types";
import Button from "../../components/Button/Button";
import "./PageFavorites.scss";
import "../../components/ProductCard/ProductCard"
import "../../components/ProductList/ProductList"

export default function PageFavorites({ favoriteItems, handleItemFavoriteClose }) {
    const productsInFavorites = favoriteItems?.map((item) => {
        return (
            <div key={item.id} className="product-card">
                <div className="image-wrapper">
                    <img width='280px' height='220px' src={item.image} alt={item.title} />
                </div>
                <h3 className="product-card-title">{item.title}</h3>

                <div className="favorites-card-info">
                    <span className="product-price">{item.price} $</span>
                    <Button
                        className="add-favorite"
                        onClick={() => handleItemFavoriteClose(item)}
                    >
                        <svg xmlns="http://www.w3.org/2000/svg" width="24" height="24" viewBox="0 0 24 24" fill="#FFD700"
                            stroke="currentColor" strokeWidth="1.5" strokeLinecap="round"
                            strokeLinejoin="round" className="feather feather-heart">
                            <path d="M20.84 4.61a5.5 5.5 0 0 0-7.78 0L12 5.67l-1.06-1.06a5.5 5.5 0 0 0-7.78 7.78l1.06 1.06L12 21.23l7.78-7.78 1.06-1.06a5.5 5.5 0 0 0 0-7.78z" />
                        </svg>
                    </Button>

                </div>
            </div>
        );
    });

    return (
        <div className="favorites-page">
            {productsInFavorites.length === 0 ? (
                <div className="favorites-page-no-items">
                    <svg className="favorites-page-no-items-icon" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="120" height="120" viewBox="0 0 520 520" xmlSpace="preserve">
                        <g>
                            <path d="M31.8,19.5c-7.2-7.3-19-7.3-26.3-0.1c-7.3,7.2-7.3,19-0.1,26.3l425.7,428.2c3.5,3.5,8.2,5.5,13.2,5.5s9.7-1.9,13.2-5.4   c7.2-7.2,7.3-19,0-26.3L31.8,19.5z" />
                            <path d="M227.9,119.4c10,9.7,18.5,20.9,25.3,33.1c4.6,9.2,15.8,12.9,25,8.3c3.6-1.8,6.5-4.7,8.3-8.3c6.8-12.2,15.3-23.4,25.3-33.2   c21.1-20.5,44.9-31,70.8-31c50.2,0,91.5,42.3,92,94.3c0.6,62.5-26.5,115.8-66.8,162.3l26.3,26.5c3.5-4,6.6-7.7,9.5-11.2   c46.5-56.7,68.8-115,68.2-178c-0.5-72.2-58.5-131-129.1-131c-56,0-93.1,34.4-112.8,59.3c-19.6-25-56.8-59.3-112.8-59.3   c-12.2,0-24.4,1.8-36.1,5.3l32.5,31.9C179.4,88.4,206.8,98.9,227.9,119.4z" />
                            <path d="M270,460.8C168,391.6,64,304.4,65.2,182.7c0.1-7.2,0.9-14.4,2.6-21.4l-29.4-29.6c-6.7,16-10.3,33.2-10.5,50.6   c-0.6,63.1,21.7,121.3,68.2,178c21.8,26.6,61.4,69.2,152.9,131.3c12.6,8.7,29.3,8.7,41.9,0c23.7-16.1,43.9-30.8,61.2-44.3   l-26.4-26.5C307.5,434.7,288.7,448.1,270,460.8z" />
                        </g>
                    </svg>
                    <p>No Items in Favorites!</p>
                </div>
            ) : (
                <div className="product-list">{productsInFavorites}</div>
            )}
        </div>
    );
}

PageFavorites.propTypes = {
    product: PropTypes.shape({
        title: PropTypes.string,
        price: PropTypes.number,
        image: PropTypes.string,
    }),
    favoriteItems: PropTypes.array,
    handleItemFavoriteClose: PropTypes.func
};
