import PropTypes from "prop-types";
import Button from "../../components/Button/Button";
import "./PageCart.scss";
import "../../components/ProductCard/ProductCard"
import "../../components/ProductList/ProductList"

export default function PageCart({ cartItems, favoriteItems, showCartModal, AddToFavorites, handleRemoveClick, }) {

    const inFavorites = (product) => {
        if (favoriteItems.some((item) => item.id === product.id)) {
            return true;
        }
        return false;
    };

    const productsInCart = cartItems?.map((item) => {
        return (

            <div key={item.id} className="product-card">
                <div className="image-wrapper">
                    <img width='280px' height='220px' src={item.image} alt={item.title} />
                </div>
                <h3 className="cart-title-modal">{item.title}</h3>
                <div className="cart-card-info">
                    <Button
                        className="remove-from-cart"
                        onClick={() => showCartModal()}
                    >
                        Change
                    </Button>
                    <span className="product-price">{item.price} $</span>
                </div>

            </div>
        );
    });

    return (
        <div className="cart-page">
            {productsInCart.length === 0 ? (
                <div className="cart-page-no-items">
                    <svg className="cart-page-no-items-icon" xmlns="http://www.w3.org/2000/svg" xmlnsXlink="http://www.w3.org/1999/xlink" version="1.1" id="Layer_1" x="0px" y="0px" width="120" height="120" viewBox="0 0 520 520" xmlSpace="preserve">
                        <g>
                            <path d="M250.52,159.08c-6.67-6.67-17.49-6.67-24.17,0s-6.67,17.49,0,24.17l39.15,39.12l-39.15,39.12   c-6.67,6.67-6.67,17.49,0,24.17c6.67,6.67,17.49,6.67,24.17,0l39.12-39.15l39.12,39.15c6.67,6.67,17.49,6.67,24.17,0   c6.67-6.67,6.67-17.49,0-24.17l-39.15-39.12l39.15-39.12c6.67-6.67,6.67-17.49,0-24.17c-6.67-6.67-17.49-6.67-24.17,0l-39.12,39.15   L250.52,159.08z" />
                            <path d="M16.57,0.49C7.14,0.49-0.5,8.13-0.5,17.56s7.64,17.07,17.07,17.07h37.89l13.69,54.85l51.13,272.56   c1.51,8.07,8.55,13.92,16.76,13.93h34.13c-37.7,0-68.27,30.56-68.27,68.27s30.56,68.27,68.27,68.27s68.27-30.56,68.27-68.27   s-30.56-68.27-68.27-68.27h238.94c-37.7,0-68.27,30.56-68.27,68.27s30.56,68.27,68.27,68.27s68.27-30.56,68.27-68.27   s-30.56-68.27-68.27-68.27h34.13c8.21-0.01,15.25-5.86,16.76-13.93l51.2-273.07c1.73-9.26-4.37-18.18-13.64-19.92l-3.12-0.29H98.15   L84.32,13.43C82.43,5.83,75.6,0.5,67.77,0.49H16.57z M150.2,341.84L105.38,102.9H473.9l-44.82,238.94H150.2z M204.3,444.24   c0,18.85-15.28,34.13-34.13,34.13s-34.13-15.28-34.13-34.13s15.28-34.13,34.13-34.13S204.3,425.39,204.3,444.24z M443.24,444.24   c0,18.85-15.28,34.13-34.13,34.13s-34.13-15.28-34.13-34.13s15.28-34.13,34.13-34.13S443.24,425.39,443.24,444.24z" />
                        </g>
                    </svg>
                    <p>Your Cart is Empty!</p>
                </div>
            ) : (
                <div className="product-list">{productsInCart}</div>
            )}
        </div>
    );
}

PageCart.propTypes = {
    id: PropTypes.number,
    product: PropTypes.object,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    inFavorites: PropTypes.func,
    handleRemoveClick: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
};
