import cn from "classnames";
import PropTypes from "prop-types";
import "./Modal.scss";

export default function Modal({ header, classNames, totalPrice, text, actions, closeModal, handleOutsideClick }) {
    
    return (
        <div className="modal-wrapper" onClick={handleOutsideClick}>
            <div className={cn("modal", classNames)}>
                <div className="modal-content">
                    <div className="modal-header">
                        <h2>{header}</h2>
                        <div className="modal-close" onClick={closeModal}>
                            <svg width="16" height="16" viewBox="0 0 22 22" fill="none" xmlns="http://www.w3.org/2000/svg">
                                <path d="M18 4L4 18M18 18L4 4.00001" stroke="#3C4242" strokeWidth="1.5" strokeLinecap="round" />
                            </svg>
                        </div>
                    </div>
                    <div className="body-text">{text}</div>
                    <div className="modal-body">{actions}</div>
                    <div className="modal-footer">{totalPrice}</div>
                </div>
            </div>
        </div>
    );
}

Modal.propTypes = {
    header: PropTypes.string,
    classNames: PropTypes.string,
    totalPrice: PropTypes.any,
    text: PropTypes.any,
    actions: PropTypes.any,
    closeModal: PropTypes.func,
    handleOutsideClick: PropTypes.func,
};
