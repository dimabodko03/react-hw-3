import PropTypes from 'prop-types';

export default function Button({ type, classNames, onClick, children }) {
    return (
        <button type={type} className={classNames} onClick={onClick}>
            {children}
        </button>
    );
}

Button.defaultProps = {
    type: "button",
    classNames: "",
    onClick: () => {},
};

Button.propTypes = {
    classNames: PropTypes.string,
    children: PropTypes.any,
    onClick: PropTypes.func,
};
