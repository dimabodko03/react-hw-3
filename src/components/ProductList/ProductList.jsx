import PropTypes from "prop-types";
import ProductCard from "../ProductCard/ProductCard";

import "./ProductList.scss";

export default function ProductList({ products, cartItems, favoriteItems, AddToCart, AddToFavorites, showCartModal, CloseCart }) {
    return (
        <div className="product-list">
            {products.map((product) => (
                <ProductCard
                    key={product.id}
                    product={product}
                    inCart={cartItems.some(
                        (item) => item.id === product.id
                    )}
                    inFavorites={favoriteItems.some(
                        (item) => item.id === product.id
                    )}
                    AddToCart={AddToCart}
                    AddToFavorites={AddToFavorites}
                    showCartModal={showCartModal} 
                    CloseCart={CloseCart}
                />
            ))}
        </div>
    );
}

ProductList.defaultProps = {
    products: [],
    cartItems: [],
    favoriteItems: [],
};

ProductList.propTypes = {
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCart: PropTypes.func,
    CloseCart: PropTypes.func
};
