import { useEffect, useState } from "react";
import PropTypes from "prop-types";
import { Route, Routes } from "react-router-dom";
import ProductList from "./components/ProductList/ProductList";
import Modal from "./components/Modal/Modal";
import Button from "./components/Button/Button"
import Header from "./components/Header/Header";
import PageCart from "./pages/PageCart//PageCart";
import PageFavorites from "./pages/PageFavorites/PageFavorites";

import "./App.css";

export default function App() {
    const [products, setProducts] = useState([]);
    const [cartItems, setCartItems] = useState(JSON.parse(localStorage.getItem("cartItems")) || []);
    const [favoriteItems, setFavoriteItems] = useState(JSON.parse(localStorage.getItem("favoriteItems")) || []);
    const [showCartModal, setShowCartModal] = useState(false);
    const [itemToRemove, setItemToRemove] = useState([null]);
    const [showConfirmationModal, setShowConfirmationModal] = useState(false);

    useEffect(() => {
        const fetchProducts = async () => {
            try {
                const response = await fetch('/products.json');
                if (!response.ok) {
                    throw new Error('Failed to fetch products');
                }
                const data = await response.json();
                setProducts(data);
            } catch (error) {
                console.error('Error fetching products:', error);
            }
        };

        fetchProducts();
    }, []);

    const handleAddToCart = (product) => {
        const indexItem = cartItems.findIndex((item) => item.id === product.id);

        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                title: product.title,
                price: product.price,
                image: product.image,
                quantity: 1,
            };
            const inCartItems = [...cartItems, newItem];
            setCartItems(inCartItems);
            localStorage.setItem("cartItems", JSON.stringify(inCartItems));
        } else {
            const updatedItem = {
                ...cartItems[indexItem],
                quantity: cartItems[indexItem].quantity + 1,
            };
            const updatedCartItems = [...cartItems];
            updatedCartItems.splice(indexItem, 1, updatedItem);

            setCartItems(updatedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
        }
    };


    const handleRemoveFromCart = (product) => {
        const updatedCartItems = [...cartItems];

        const indexItem = updatedCartItems.findIndex(
            (item) => item.id === product.id
        );

        if (indexItem !== -1) {
            updatedCartItems[indexItem].quantity -= 1;

            if (updatedCartItems[indexItem].quantity === 0) {
                updatedCartItems.splice(indexItem, 1);
            }

            setCartItems(updatedCartItems);
            localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
        }
    };

    const handleAddToFavorites = (product) => {
        const indexItem = favoriteItems.findIndex((item) => item.id === product.id);

        if (indexItem === -1) {
            const newItem = {
                id: product.id,
                title: product.title,
                price: product.price,
                image: product.image,
            };
            const inFavoriteItems = [...favoriteItems, newItem];
            setFavoriteItems(inFavoriteItems);
            localStorage.setItem("favoriteItems", JSON.stringify(inFavoriteItems));
        } else {
            const updatedFavoriteItems = favoriteItems.filter((item) => item.id !== product.id);
            setFavoriteItems(updatedFavoriteItems);
            localStorage.setItem("favoriteItems", JSON.stringify(updatedFavoriteItems));
        }
    };

    const handleToggleCartModal = () => {
        setShowCartModal(!showCartModal);
    };

    const ConfirmationModal = (item) => {
        setItemToRemove(item);
        setShowConfirmationModal(!showConfirmationModal);
    };

    const handleOutsideCartModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            handleToggleCartModal();
        }
    };

    const handleOutsideConfirmationModalClick = (event) => {
        if (!event.target.closest(".modal")) {
            ConfirmationModal();
        }
    };

    const removeItemFromCart = (product) => {
        const updatedCartItems = cartItems.filter((item) => item.id !== product.id);
        setCartItems(updatedCartItems);
        localStorage.setItem("cartItems", JSON.stringify(updatedCartItems));
        console.log(updatedCartItems);
    };

    const handleItemFavoriteClose = (product) => {
        const updatedFavoriteItems = favoriteItems.filter((item) => item.id !== product.id);
        setFavoriteItems(updatedFavoriteItems);
        localStorage.setItem("favoriteItems", JSON.stringify(updatedFavoriteItems));
    };

    const handleRemoveClick = (item) => {
        setItemToRemove(item);
        ConfirmationModal();
    };

    const handlerConfirmRemove = () => {
        if (itemToRemove) {
            removeItemFromCart(itemToRemove);
        }
        ConfirmationModal();
    };

    const cartItemTotal = cartItems.reduce((total, item) => total + item.quantity, 0);
    const totalPrice = cartItems.reduce((total, item) => total + item.price * item.quantity, 0);

    return (
        <div className="app-wrapper">
            <Header
                cartItems={cartItems}
                favoriteItems={favoriteItems}
                cartItemTotal={cartItemTotal}
                handleToggleCartModal={handleToggleCartModal}
            />
            <Routes>
                <Route
                    path="/"
                    element={
                        <ProductList
                            products={products}
                            cartItems={cartItems}
                            favoriteItems={favoriteItems}
                            AddToCart={handleAddToCart}
                            AddToFavorites={handleAddToFavorites}
                            showCartModal={handleToggleCartModal}
                        />
                    }
                />
                <Route
                    path="/favorites"
                    element={
                        <PageFavorites
                            favoriteItems={favoriteItems}
                            handleItemFavoriteClose={handleItemFavoriteClose}
                        />
                    }
                />
                <Route
                    path="/cart"
                    element={
                        <PageCart
                            cartItems={cartItems}
                            favoriteItems={favoriteItems}
                            showCartModal={handleToggleCartModal}
                            AddToFavorites={handleAddToFavorites}
                            handleRemoveClick={handleRemoveClick}
                        />
                    }
                />

            </Routes>
            {showCartModal && (
                <Modal
                    classNames="shoping-cart-modal"
                    cartItems={cartItems}
                    closeButton={handleToggleCartModal}
                    closeModal={handleToggleCartModal}
                    handleOutsideClick={handleOutsideCartModalClick}
                    header={'Cart'}
                    text={!cartItems.length && <h2>Your Cart is Empty!</h2>}
                    actions={cartItems.map((item) => (
                        <div className="cart-modal-wrapper" key={item.id}>
                            <div className="cart-text-modal">
                                <div className="cart-title-modal">
                                    {item.title}
                                </div>
                                <div className="cart-price-modal">
                                    {item.price} $
                                </div>
                            </div>
                            <div className="plus-minus-btn-wrapper">
                                <Button
                                    onClick={() => handleRemoveFromCart(item)}
                                >
                                    -
                                </Button>
                                <div className="quantity-product">
                                    {item.quantity}
                                </div>
                                <Button
                                    onClick={() => handleAddToCart(item)}
                                >
                                    +
                                </Button>
                                <Button
                                    onClick={() => ConfirmationModal(item)}
                                >
                                    &times;
                                </Button>
                            </div>
                        </div>
                    ))}


                    totalPrice={
                        cartItems.length && (
                            <div className="total-price">
                                Total: {totalPrice}  $
                            </div>
                        )
                    }
                />
            )}

            {showConfirmationModal && (
                <Modal
                    closeModal={() => ConfirmationModal()}
                    handleOutsideClick={(event) =>
                        handleOutsideConfirmationModalClick(event)
                    }
                    text={
                        <h2>
                            Are You Sure?
                        </h2>
                    }
                    actions={
                        <div className="modal-buttons-wrapper">
                            <Button
                                onClick={() => {
                                    handlerConfirmRemove(itemToRemove);
                                    ConfirmationModal();
                                }}
                            >
                                Yes, Delete
                            </Button>
                            <Button
                                onClick={() => ConfirmationModal()}
                            >
                                No
                            </Button>
                        </div>
                    }
                />
            )}
        </div>
    );
}

App.propTypes = {
    products: PropTypes.array,
    cartItems: PropTypes.array,
    favoriteItems: PropTypes.array,
    AddToCart: PropTypes.func,
    AddToFavorites: PropTypes.func,
    showCartModal: PropTypes.func,
    closeModal: PropTypes.bool,
    handleOutsideClick: PropTypes.func,
    handleAddToCart: PropTypes.func,
    totalPrice: PropTypes.func
};